# Hacktivisme

Pour mieux comprendre qui je suis, et ce que je fais
http://pascal.kotte.net

De façon gégérale, si tu es DEV ou simple curieux numérique, engagé dans la transition durable et responsable, actif sur la Suisse romande et France voisine, merci de me connecter et me raconter ce que tu fais.

Si tu as un projet/idée/organisme pour alimenter des révolutions de la transition durable, ou responsable:
* numérique [PaKo](http://callme.kotte.net)
* pas numérique [LaTransition.ch](http://LaTransition.ch)

Tu as des questions [Me contacter](http://callme.kotte.net)